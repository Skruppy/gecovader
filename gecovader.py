#!/usr/bin/env python3
## This file is part of gecovader <https://gitlab.com/Skrupellos/gecovader>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import csv
import datetime
import argparse
import requests
import requests_cache



now = datetime.datetime.now().date()


def load_url(url):
	response = requests.get(url)
	response.raise_for_status()
	if response.from_cache:
		print(f'Using cached response for {url}')
	return response.text


## https://service.destatis.de/bevoelkerungspyramide/#!y=2021
def load_ages(acceptance=1.0, variant=1):
	## row[0]: Simulation variant
	## row[1]: Year
	## row[2]: Male / Female ("m"/"w")
	## row[3]: Population total
	## row[4:]: Population by age
	genders = []
	csvfile = csv.reader(
		load_url('https://service.destatis.de/bevoelkerungspyramide/data/14_bevoelkerungsvorausberechnung_daten.csv').splitlines(),
		delimiter=';'
	)
	hdr = next(csvfile)
	for row in csvfile:
		if int(row[0]) == variant and int(row[1]) == now.year:
			genders.append([int(x)*1000*acceptance for x in row[4:]])
	
	return [a+b for a, b in zip(*genders)]


## https://impfdashboard.de/daten
def load_stats():
	rows = []
	csvfile = csv.reader(
		load_url('https://impfdashboard.de/static/data/germany_vaccinations_timeseries_v2.tsv').splitlines(),
		delimiter='\t'
	)
	hdr = next(csvfile)
	for row in csvfile:
		rows.append(data := {})
		for k, v in zip(hdr, row):
			if k == 'date':
				data[k] = datetime.date(*map(int, v.split('-')))
			elif '.' in v:
				data[k] = float(v)
			else:
				data[k] = int(v)
	
	return rows


## https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Projekte_RKI/covimo_studie_Ergebnisse.html
## https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Projekte_RKI/COVIMO_Reports/covimo_studie_bericht_3.pdf?__blob=publicationFile
def covimo_acceptance(level):
	acceptance_list = [0.726, 0.086, 0.107, 0.036, 0.044]
	
	if level < 0:
		return 0
	if level >= len(acceptance_list):
		return 1
	
	a = int(level)
	b = level - a
	return sum(acceptance_list[0:a]) + b * acceptance_list[a]


def guess_age_group(ppl_by_age, ppl_vaccinated):
	for age in range(len(ppl_by_age)):
		total = sum(ppl_by_age[age:])
		if total < ppl_vaccinated:
			## if there are more ppl vaccinated than ppl aged $age or older,
			## than ppl aged $age - 1 will be currently vaccinated.
			return age - 1
	
	return len(ppl_by_age) - 1


def main():
	parser = argparse.ArgumentParser(description='Gecovader - German Coronavirus Vaccination Date Estimator\nGuestimates the CoVid-19 vaccination date for Germany, based on your age and the current live data.', formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('age', type=int, help='The age you want to forecast the vaccination date for.')
	parser.add_argument('-a', '--acceptance', type=float, help='Vaccination acceptance rate (float between 0 and 1). If not specified, the acceptance rate will be based on --level.')
	parser.add_argument('-l', '--level', default=2.5, type=float, help='''Calculate acceptance rate based on the COVIMO study (float between between 0 and 5. default: 2.5).
  0.0: No one
  1.0: Only ppl realy wanting
  2.0: Only ppl wanting
  2.5: Ppl wanting and 50%% of undecided
  3.0: Ppl wanting and undecided
  4.0: Also ppl probably not wanting
  5.0: Also ppl definitely not wanting (so, everyone)''')
	parser.add_argument('-v', '--vaccinated', type=int, help='Number of people already vaccinated (default: actual value from stats).')
	parser.add_argument('-d', '--daily', type=int, help='Vaccinations per day (default: from actual 7 day average from stats).')
	args = parser.parse_args()
	
	requests_cache.install_cache('/tmp/gecovader', backend='sqlite', expire_after=3600)
	
	if args.acceptance is not None:
		acceptance = args.acceptance
	else:
		acceptance = covimo_acceptance(args.level)
	
	ppl_by_age = load_ages(acceptance)
	stats = load_stats()
	stats_date = stats[-1]['date']
	
	if args.vaccinated is None:
		ppl_vaccinated = stats[-1]['personen_erst_kumulativ']
	else:
		ppl_vaccinated = args.vaccinated
	
	if args.daily is None:
		vaccinations_per_day = sum([x['dosen_erst_differenz_zum_vortag'] for x in stats[-7:]])/7
	else:
		vaccinations_per_day = args.daily
	
	age = args.age
	
	current_age = guess_age_group(ppl_by_age, ppl_vaccinated)
	
	ppl_older = sum(ppl_by_age[age+1:])
	ppl_same_age = ppl_by_age[age]
	
	ppl_before_min = ppl_older - ppl_vaccinated
	ppl_before_max = ppl_before_min + ppl_same_age
	
	waiting_days_min = ppl_before_min / vaccinations_per_day
	waiting_days_max = ppl_before_max / vaccinations_per_day
	group_days = ppl_same_age / vaccinations_per_day
	
	start_date = stats_date + datetime.timedelta(days=waiting_days_min)
	end_date = stats_date + datetime.timedelta(days=waiting_days_max)
	
	print(f'''{ppl_vaccinated:,} people got their first shot until {stats_date}.
If {acceptance:.2%} accept the vaccine, people aged {current_age} should get their first shot currently.
There are {ppl_before_min:,.0f} - {ppl_before_max:,.0f} people before you.
Hence you have to wait {(start_date-now).days:.0f} - {(end_date-now).days:.0f} days ({start_date} - {end_date}) until you get your first shot, if {vaccinations_per_day:,.0f} people per day get their first shot.
Your age group contains {ppl_same_age:,.0f} people which takes {group_days:.1f} days to vaccinate.''')


if __name__ == '__main__':
	main()